package com.oauth.practice.Oauth.imp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthImpApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthImpApplication.class, args);
	}

}
