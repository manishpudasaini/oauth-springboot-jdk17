package com.oauth.practice.Oauth.imp.repo;

import com.oauth.practice.Oauth.imp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User,Integer> {

    @Query(nativeQuery = true,value = "select * from users where username = ?1")
    Optional<User> findByUsername(String name);
}
