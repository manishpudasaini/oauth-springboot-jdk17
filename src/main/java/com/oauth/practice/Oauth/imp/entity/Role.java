package com.oauth.practice.Oauth.imp.entity;


import jakarta.persistence.*;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "role",
        uniqueConstraints = {@UniqueConstraint(name = "uk_role_name",columnNames = "name")})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role {
    @Id
    @GeneratedValue(generator = "user_role_seq_gen",strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "user_role_seq_gen",sequenceName = "user_role_seq",allocationSize = 1)
    private Integer id;

    @Column(name = "name")
    private String name;
}
